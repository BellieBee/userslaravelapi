<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login', 'App\Http\Controllers\LoginController@login');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('get-users', 'App\Http\Controllers\ApiControllers\UsersController@getUsers');
    Route::post('save-user', 'App\Http\Controllers\ApiControllers\UsersController@saveUser');
    Route::get('get-user/{id}', 'App\Http\Controllers\ApiControllers\UsersController@getUser');
    Route::get('delete-user/{id}', 'App\Http\Controllers\ApiControllers\UsersController@deleteUser');
    Route::get('get-users-between/{date_initial}/{date_final}', 'App\Http\Controllers\ApiControllers\UsersController@getUsersBetween');
});
