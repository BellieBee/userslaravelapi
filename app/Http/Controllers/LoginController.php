<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class LoginController extends Controller
{
    public function login(Request $request) {
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response([
                'message' => 'These credentials do not match our records.',
                'used_user' => $request->email,
                'used_password' => $request->password,
                'hashed_password' => Hash::make($request->password)
            ], 404);
        }
        $token = $user->createToken('my-app-token')->accessToken;
        $response = ['user' => $user, 'token' => $token];
        return response($response, 200);
    }
}
