<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    public function getUsers() {
        $users = User::with('profile')->get();
        return response()->json(['users' => $users], 200);
    }

    public function saveUser(Request $request) {
        $user = User::updateOrCreate(['id' => $request->id], $request->all());
        if($request->hasFile('lma_profile')) {
            $fileName = uniqid().$request->lma_profile->getClientOriginalName();
            $url_file = $request->lma_profile->storeAs('user', $fileName, 'public');
            $user->profile()->updateOrCreate(['id' => $request->profile_id],[
                'lma_profile' => $url_file,
            ]);
        }
        return response()->json(['user' => $user, 'profile' => $url_file], 201);
    }

    public function getUser($id) {
        $user = User::with('profile')->find($id);
        return response()->json(['user' => $user], 201);
    }

    public function deleteUser($id) {
        $user = User::find($id);
        $user->delete();
        return response()->json($user, 200);
    }

    public function getUsersBetween($date_initial, $date_final) {
        $users = User::with('profile')->whereBetween('created_at', [$date_initial, $date_final])->get();
        return response()->json($users, 200);
    }
}
